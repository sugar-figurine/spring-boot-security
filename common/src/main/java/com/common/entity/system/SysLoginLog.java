package com.common.entity.system;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.common.entity.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ApiModel(description = "SysLoginLog")
@TableName("sys_login_log")
@ToString
public class SysLoginLog extends Model<SysLoginLog> {

	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	private Long id;

	@TableField("create_time")
	private Date createTime;

	@TableField("update_time")
	private Date updateTime;

	@TableLogic  //逻辑删除 默认效果 0 没有删除 1 已经删除
	@TableField("is_deleted")
	private Integer isDeleted;


	@ApiModelProperty(value = "用户账号")
	@TableField("username")
	private String username;

	@ApiModelProperty(value = "登录IP地址")
	@TableField("ipaddr")
	private String ipaddr;

	@ApiModelProperty(value = "登录状态（0成功 1失败）")
	@TableField("status")
	private Integer status;

	@ApiModelProperty(value = "提示信息")
	@TableField("msg")
	private String msg;

	@ApiModelProperty(value = "访问时间")
	@TableField("access_time")
	private Date accessTime;

}