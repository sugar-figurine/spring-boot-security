package com.common.entity.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.common.entity.base.BaseEntity;
import lombok.Data;


@Data
@TableName("sys_role")
public class SysRole extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	@TableField("role_name")
	private String roleName;

	@TableField("role_code")
	private String roleCode;

	@TableField("description")
	private String description;

	@Override
	public String toString() {
		return "SysRole{" +
				"roleName='" + roleName + '\'' +
				", roleCode='" + roleCode + '\'' +
				", description='" + description + '\'' +
				'}';
	}
}

