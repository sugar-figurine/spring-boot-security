package com.mds.system;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.entity.system.SysLoginLog;
import com.common.entity.system.SysMenu;
import com.common.entity.system.SysOperLog;
import com.common.entity.system.SysRole;
import com.mds.system.mapper.SysMenuMapper;
import com.mds.system.mapper.SysRoleMapper;
import com.mds.system.service.SysMenuService;
import com.mds.system.service.SysRoleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.management.relation.Role;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
public class SysRoreTest {

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    SysRoleMapper roleMapper;

    @Autowired
    SysRoleService roleService;

    @Autowired
    SysMenuService service;

    @Test
    public void testListRole(){
        List<SysRole> list = roleService.list();
        list.forEach(System.out::println);
    }

    @Test
    public void testAll(){
        Page<SysRole> page = new Page<>(1, 2);
        IPage<SysRole> sysRolePage = roleMapper.selectPage(page, null);
        System.out.println("sysRolePage = " + sysRolePage.getRecords());
        //List<SysRole> sysRoles = roleMapper.selectList(null);
        //sysRoles.forEach(System.out::println);

    }

    @Test
    public void add(){
        SysRole sysRole = new SysRole();
        sysRole.setRoleName("测试管理员");
        sysRole.setRoleCode("test");
        sysRole.setDescription("测试管理员");
        int insert = roleMapper.insert(sysRole);
        System.out.println("insert = " + insert);
    }

    @Test
    public void delete(){
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        wrapper.eq("id", "1592768511669891074");
        int delete = roleMapper.delete(wrapper);

    }

    @Test
    public void encoder(){
        String encode = encoder.encode("123456");
        System.out.println(encode);

    }

    @Test
    public void fandMenuListByUserId(){
        List<SysMenu> sysMenus = service.fandMenuListByUserId("1");
        System.out.println(JSON.toJSONString(sysMenus));
    }

    @Test
    public void testAR(){
        SysOperLog sysOperLog = new SysOperLog();
        List<SysOperLog> sysOperLogs = sysOperLog.selectAll();
        //System.out.println(sysOperLogs);
        SysLoginLog sysLoginLog = new SysLoginLog();
        List<SysLoginLog> sysLoginLogs = sysLoginLog.selectAll();
        System.out.println(sysLoginLogs);
    }

}
