package com.mds.system.config;

import com.mds.system.config.security.*;
import com.mds.system.config.security.filter.TokenFilter;
import com.mds.system.config.security.handler.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Resource
    private LoginSuccessHandler successHandler;

    @Resource
    private LoginFailureHandler failureHandler;

    @Resource
    private CustomerAccessDeniedHandler accessDeniedHandler;

    @Resource
    private AnonymousAuthenticationHandler authenticationHandler;

    @Resource
    private CustomUserDetailService userDetailService;

    @Resource
    private TokenFilter tokenFilter;

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    public static final String[] URL_WHITE_LIST ={
            "/admin/sysRole/index/login",
            "/favicon.ico"
            ,"/swagger-resources/**"
            , "/webjars/**"
            , "/v2/**"
            , "/swagger-ui.html/**"
            , "/doc.html"
    };

    /**
     * 处理登录认证
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // token认证
        http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);
        // 开启跨域 crsf关闭
        http.cors()
            .and()
                .csrf().disable()
            // 登入登出
                .formLogin()
                .loginProcessingUrl("/admin/sysRole/index/login")
                // 登录成功处理器
                .successHandler(successHandler)
                // 登录失败处理器
                .failureHandler(failureHandler)
                .and()
                // 退出登录，
                .logout()
                // 退出成功处理
                //.logoutSuccessHandler()
            // 关闭session
            .and()
                // 会话管理
                .sessionManagement()
                // 会话策略
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

            // 拦截规则
            //.and()
                //.authorizeRequests()
                // 白名单放行
                //.antMatchers(URL_WHITE_LIST).permitAll()
                // 其他请求一律拦截，进行身份验证
                //.anyRequest().authenticated()
            // 异常处理
            .and()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler)  // 认证的用户无访问权限
                .authenticationEntryPoint(authenticationHandler);  // 匿名无访问权限
    }

    /**
     * 配置认证处理器
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

}
