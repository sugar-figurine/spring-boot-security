package com.mds.system.config;

import com.common.entity.result.Result;
import com.mds.system.exception.AuthException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 */
@RestControllerAdvice
public class ExceptionHandling {

    @ExceptionHandler(Exception.class)
    public Result error(Exception e){
        return Result.fail();
    }

    @ExceptionHandler(AuthException.class)
    public Result auth(AuthException e){
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public Result runtime(AuthException e){
        return Result.fail(e.getMessage());
    }

}
