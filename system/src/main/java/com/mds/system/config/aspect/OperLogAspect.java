package com.mds.system.config.aspect;

import com.alibaba.fastjson.JSON;
import com.common.entity.system.SysOperLog;
import com.mds.system.annotation.Log;
import com.mds.system.enums.BusinessType;
import com.mds.system.utils.IpUtil;
import com.mds.system.utils.JwtHelper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 保存操作日志
 */
@Aspect
@Component
public class OperLogAspect {

    @AfterReturning(pointcut = "@annotation(controllerLog)", returning = "jsonResult")
    public void after(JoinPoint joinPoint, Log controllerLog, Object jsonResult){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("token");
        String username = JwtHelper.getUsername(token);
        SysOperLog operLog = new SysOperLog();
        // 操作的用户名
        operLog.setOperName(username);
        // 模块名称
        operLog.setTitle(controllerLog.title());
        // 业务类型
        operLog.setBusinessType(controllerLog.businessType().name());
        operLog.setOperatorType(controllerLog.operatorType().name());
        // 请求方式
        operLog.setRequestMethod(request.getMethod());
        // 操作的方法
        operLog.setMethod(joinPoint.getTarget().getClass().getName()+"."+joinPoint.getSignature().getName()+"()");
        // 请求的rui
        operLog.setOperUrl(request.getRequestURI());
        // ip地址
        operLog.setOperIp(IpUtil.getIpAddress(request));
        // 返回的结果
        operLog.setJsonResult(JSON.toJSONString(jsonResult));
    }


}
