package com.mds.system.config.security.handler;

import com.alibaba.fastjson.JSON;
import com.common.entity.result.Result;
import com.common.entity.system.SysLoginLog;
import com.mds.system.config.security.CustomUser;
import com.mds.system.config.security.SysUserDetail;
import com.mds.system.utils.IpUtil;
import com.mds.system.utils.JwtHelper;
import com.mds.system.utils.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * 登录成功处理器
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    RedisService redisService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();

        CustomUser customUser = (CustomUser) authentication.getPrincipal();
        // 生成token
        String token = JwtHelper.createToken(customUser.getSysUser().getId().toString(), customUser.getSysUser().getUsername());
        // 保存到redis中
        redisService.set("token_"+token, token, JwtHelper.tokenExpiration);
        // 保存登录信息
        SysLoginLog sysLoginLog = new SysLoginLog();
        sysLoginLog.setMsg("登录成功");
        // 登录用户名
        sysLoginLog.setUsername(customUser.getSysUser().getUsername());
        // 登录的ip地址
        sysLoginLog.setIpaddr(IpUtil.getIpAddress(request));
        // 保存到数据库
        sysLoginLog.insert();
        HashMap<String, Object> map = new HashMap<>();
        map.put("token", token);
        Result<HashMap<String, Object>> ok = Result.ok(map);
        outputStream.write(JSON.toJSONString(ok).getBytes());
        outputStream.flush();
        outputStream.close();

    }
}
