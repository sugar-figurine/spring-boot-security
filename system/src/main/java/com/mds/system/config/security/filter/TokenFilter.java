package com.mds.system.config.security.filter;

import com.mds.system.config.SecurityConfig;
import com.mds.system.config.security.CustomUserDetailService;
import com.mds.system.config.security.handler.LoginFailureHandler;
import com.mds.system.exception.CutomAuthenticationException;
import com.mds.system.utils.JwtHelper;
import com.mds.system.utils.RedisService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import sun.plugin.liveconnect.SecurityContextHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 验证token
 */
@Data
@Component
public class TokenFilter extends OncePerRequestFilter {

    @Value("${loginUri}")
    String loginUri;

    @Autowired
    RedisService redisService;

    @Autowired
    CustomUserDetailService userDetailService;

    @Autowired
    LoginFailureHandler loginFailureHandler;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        // 获取当前请求接口
        String requestURI = request.getRequestURI();
        // 放行白名单
        String[] urlWhiteList = SecurityConfig.URL_WHITE_LIST;
        List<String> list = Arrays.asList(urlWhiteList);
        // 登录，登出，doc.html等直接放行
        if (!list.contains(requestURI)){
            try {
                // 验证token
                verifyToken(request);
            }catch (AuthenticationException e){
                loginFailureHandler.onAuthenticationFailure(request, response, e);
            }
        }
        doFilter(request, response, filterChain);
    }

    private void verifyToken(HttpServletRequest request) {
        // 获取前端传入的token
        // 从请求头中获取
        String token = request.getHeader("token");
        // 判断是否为空，如果为空，通过参数获取
        if (StringUtils.isEmpty(token)){
            // 从请求参数中获取
            token = request.getParameter("token");
        }
        // 参数也获取不了，说明没有token传递
        if (StringUtils.isEmpty(token)){
            throw new CutomAuthenticationException("token不存在");
        }
        // 从redis中获取token
        String redisToken = redisService.get("token_"+token);
        // 如果redis中没有token说明token过期
        if (StringUtils.isEmpty(redisToken)){
            throw new CutomAuthenticationException("token已过期");
        }
        // 比较两次token
        if (!redisToken.equals(token)){
            throw new CutomAuthenticationException("token验证失败");
        }
        // 从token中获取用户名
        String username = JwtHelper.getUsername(token);
        if (StringUtils.isEmpty(username)){
            throw new CutomAuthenticationException("token解析失败");
        }
        UserDetails userDetails = userDetailService.loadUserByUsername(username);
        if (userDetails == null){
            throw new CutomAuthenticationException("token验证失败");
        }
        //创建身份验证对象
        UsernamePasswordAuthenticationToken authenticationToken = new
                UsernamePasswordAuthenticationToken(userDetails, null,
                userDetails.getAuthorities());
        authenticationToken.setDetails(new
                WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }
}
