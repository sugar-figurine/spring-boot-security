package com.mds.system.config.security;

import com.common.entity.system.SysMenu;
import com.common.entity.system.SysUser;
import com.mds.system.service.SysMenuService;
import com.mds.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysMenuService sysMenuService;
    /**
     * 用户认证和授权
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据前端传入的用户名查询用户
        SysUser sysUser = sysUserService.fandUserByUsername(username);
        if (sysUser == null){
            throw new UsernameNotFoundException("用户不存在！");
        }
        if (sysUser.getStatus() == 0 ){
            throw new RuntimeException("用户已被禁用！");
        }
        // 根据用户id查询用户权限
        List<SysMenu> sysMenus = sysMenuService.fandMenuListByUserId(sysUser.getId().toString());
        // 收集权限码
        List<String> perms = sysMenus.stream()
                .filter(v -> v.getPerms()!=null && v.getPerms()!="")  // 过滤掉空的数据
                .map(SysMenu::getPerms)
                .filter(v -> !v.equals(""))   // 只收集不为空的
                .collect(Collectors.toList());
        String[] strings = perms.toArray(new String[perms.size()]);
        // 授权
        List<GrantedAuthority> list = AuthorityUtils.createAuthorityList(strings);
        CustomUser customUser = new CustomUser(sysUser, list);
        return customUser;

    }
}
