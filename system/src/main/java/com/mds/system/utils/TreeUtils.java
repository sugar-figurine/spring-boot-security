package com.mds.system.utils;

import com.common.entity.system.SysMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 处理树形菜单
 */
public class TreeUtils {

    /**
     * @param menuList 组装为树形结构的list
     * @return
     */
    public static List<SysMenu> handlerTree(List<SysMenu> menuList) {
        return menuList.stream()
                .map(sysMenu -> fandChildren(menuList, sysMenu))
                // 过滤，只收集一级目录
                .filter(sysMenu -> sysMenu.getParentId() == 0)
                .collect(Collectors.toList());
    }

    // 查找当前菜单的子菜单
    public static SysMenu fandChildren(List<SysMenu> menuList, SysMenu sysMenu) {
        menuList.forEach(v -> {
            if (sysMenu.getId().equals(v.getParentId())) {
                if (sysMenu.getChildren() == null) {
                    sysMenu.setChildren(new ArrayList<SysMenu>());
                }
                sysMenu.getChildren().add(v);
            }
        });
        return sysMenu;
    }

}
