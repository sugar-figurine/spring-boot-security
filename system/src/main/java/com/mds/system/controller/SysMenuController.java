package com.mds.system.controller;

import com.common.entity.result.Result;
import com.common.entity.system.SysMenu;
import com.mds.system.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/sysMenu")
@Api(tags = "菜单管理")
public class SysMenuController {

    @Autowired
    SysMenuService sysMenuService;

    // 查询所有的菜单
    @GetMapping("/menu")
    @ApiOperation("查询所有的菜单")
    public Result treeMenu(){
        List<SysMenu> sysMenus = sysMenuService.treeMenu();
        return Result.ok(sysMenus);
    }

    // 根据id查询菜单
    @GetMapping("/menu/{id}")
    @ApiOperation("根据id查询菜单")
    public Result fandMenu(@PathVariable("id") Long id){
        SysMenu byId = sysMenuService.getById(id);
        return Result.ok(byId);
    }

    // 添加菜单
    @PostMapping("/menu")
    @ApiOperation("添加菜单")
    public Result addMenu(@RequestBody SysMenu sysMenu){
        boolean save = sysMenuService.save(sysMenu);
        if (save){
            return Result.ok();
        }
        return Result.fail();
    }

    // 删除菜单
    @DeleteMapping("/menu/{id}")
    @ApiOperation("删除菜单")
    public Result deleteMenu(@PathVariable("id") Long id){
        boolean removeById = sysMenuService.removeById(id);
        if (removeById){
            return Result.ok();
        }
        return Result.fail();
    }

    // 修改菜单
    @PutMapping("/menu")
    @ApiOperation("修改菜单")
    public Result updateMenu(@RequestBody SysMenu sysMenu){
        System.out.println("sysMenu = " + sysMenu.toString());
        boolean b = sysMenuService.updateById(sysMenu);
        if (b){
            return Result.ok();
        }
        return Result.fail();
    }



}
