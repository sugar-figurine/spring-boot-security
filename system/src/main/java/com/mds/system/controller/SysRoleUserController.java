package com.mds.system.controller;

import com.common.entity.result.Result;
import com.common.entity.vo.AssginRoleVo;
import com.mds.system.service.SysUserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/admin/system")
@Api(tags = "角色分配接口")
public class SysRoleUserController {

    @Autowired
    SysUserRoleService service;

    // 根据用户id查询所有角色和当前用户拥有的角色
    @ApiOperation("根据用户id查询所有角色和当前用户拥有的角色")
    @GetMapping("/userRole/{userId}")
    public Result userRole(@PathVariable("userId") Long id){
        Map<String, Object> map = service.getUserRole(id);
        return Result.ok(map);
    }

    @ApiOperation("保存分配的角色")
    @PostMapping("/userRole")
    public Result saveUserRole(@RequestBody AssginRoleVo vo){
        service.saveUserRole(vo);
        return Result.ok();
    }

}
