package com.mds.system.controller;

import com.common.entity.result.Result;
import com.common.entity.vo.AssginMenuVo;
import com.mds.system.service.SysRoleMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/assign")
@Api(tags = "分配权限")
public class SysRoleMenuController {

    @Autowired
    SysRoleMenuService service;

    @ApiOperation("根据角色id查询角色的权限")
    @GetMapping("/auth/{id}")
    public Result roleAuth(@PathVariable("id") Long id){
        return Result.ok(service.fandAuth(id));
    }

    @PostMapping("/auth")
    public Result toAssign(@RequestBody AssginMenuVo vo){
        service.saveAssign(vo);
        return Result.ok();
    }

}
