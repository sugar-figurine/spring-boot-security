package com.mds.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.entity.result.Result;
import com.common.entity.system.SysRole;
import com.common.entity.vo.SysRoleQueryVo;
import com.mds.system.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import java.util.Arrays;
import java.util.List;

@Api(tags = "角色管理")
@RestController
@RequestMapping("/admin/sysRole")
@PreAuthorize("hasAuthority('sysRole')")
public class SysRoleController {

    @Autowired
    SysRoleService service;

    @ApiOperation("查询所有角色列表")
    @GetMapping("/findAll")
    public Result sysRoleList(){
        List<SysRole> list = service.list();
        if (list.isEmpty()){
            return Result.fail();
        }else {
            return Result.ok(list);
        }
    }

    @ApiOperation("根据Id查询角色")
    @GetMapping("/findById/{id}")
    public Result findById(
            @ApiParam(name = "id", value = "角色id", required = true)
            @PathVariable("id")Long id){
        SysRole sysRole = service.getById(id);
        if (sysRole == null){
            return Result.fail();
        }else {
            return Result.ok(sysRole);
        }
    }

    @ApiOperation("修改角色")
    @PutMapping("/update")
    public Result update(
            @ApiParam(name = "sysRole", value = "角色对象", required = true)
            @RequestBody SysRole sysRole){
        boolean isSuccess = service.updateById(sysRole);
        if (!isSuccess){
            return Result.fail();
        }else {
            return Result.ok();
        }
    }

    @ApiOperation("批量删除")
    @DeleteMapping("/removeByIds")
    public Result delete(@RequestBody String[] ids){
        boolean isSuccess = service.removeByIds(Arrays.asList(ids));
        if (isSuccess){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    @ApiOperation(value = "获取分页列表")
    @GetMapping("/{page}/{limit}")
    public Result index(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(name = "roleQueryVo", value = "查询对象", required = false)
                    SysRoleQueryVo roleQueryVo) {
        IPage<SysRole> pageModel = service.selectPage(new Page<>(page, limit), roleQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation("根据Id删除角色")
    @DeleteMapping("/remove/{id}")
    public Result removeSysRole(
            @ApiParam(name = "id", value = "角色id", required = true)
            @PathVariable("id") String id){
        boolean byId = service.removeById(id);
        if (byId){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    @ApiOperation("添加角色")
    @PostMapping("/save")
    public Result saveSysRole(
            @ApiParam(name = "sysRole", value = "角色对象", required = true)
            @RequestBody SysRole sysRole){
        System.out.println("sysRole = " + sysRole);
        boolean save = service.save(sysRole);
        if (save){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

}
