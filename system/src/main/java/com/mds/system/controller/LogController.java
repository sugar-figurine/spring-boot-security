package com.mds.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.entity.result.Result;
import com.common.entity.system.SysLoginLog;
import com.common.entity.system.SysOperLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/system/log")
public class LogController {

    @GetMapping("/operLog/{page}/{limit}")
    public Result operLog(@PathVariable("page") Long page,@PathVariable("limit") Long limit){
        Page<SysOperLog> logPage = new Page<>(page, limit);
        SysOperLog operLog = new SysOperLog();
        Page<SysOperLog> selectPage = operLog.selectPage(logPage, null);
        return Result.ok(selectPage);
    }

    @GetMapping("/loginLog/{page}/{limit}")
    public Result loginLog(@PathVariable("page") Long page,@PathVariable("limit") Long limit){
        Page<SysLoginLog> logPage = new Page<>(page, limit);
        SysLoginLog loginLog = new SysLoginLog();
        Page<SysLoginLog> selectPage = loginLog.selectPage(logPage, null);
        return Result.ok(selectPage);
    }

}
