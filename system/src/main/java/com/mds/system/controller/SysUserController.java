package com.mds.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.common.entity.result.Result;
import com.common.entity.system.SysUser;
import com.common.entity.vo.SysUserQueryVo;
import com.mds.system.annotation.Log;
import com.mds.system.enums.BusinessType;
import com.mds.system.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/sysUser")
@Api(tags = "系统用户管理接口")
public class SysUserController {

    @Autowired
    SysUserService userService;

    @Autowired
    BCryptPasswordEncoder encoder; // 密码加密

    // 查询用户
    @GetMapping("user/{id}")
    @ApiOperation(value = "根据ID查询用户")
    public Result fandUserById(@PathVariable("id") Long id){
        SysUser sysUser = userService.getById(id);
        if (sysUser != null){
            return Result.ok(sysUser);
        }else {
            return Result.fail();
        }
    }

    // 修改用户
    @PutMapping("user")
    @ApiOperation(value = "修改用户")
    @PreAuthorize("hasAuthority('bnt.sysUser.update')")
    public Result updateUser(@RequestBody SysUser user){
        boolean update = userService.updateById(user);
        if (update){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 添加用户
    @PostMapping("user")
    @ApiOperation(value = "添加用户")
    @Log(title = "用户管理",businessType = BusinessType.INSERT)
    @PreAuthorize("hasAuthority('bnt.sysUser.add')")
    public Result addUser(@RequestBody SysUser user){
        // 密码加密
        user.setPassword(encoder.encode(user.getPassword()));
        boolean save = userService.save(user);
        if (save){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 删除用户
    @DeleteMapping("user/{id}")
    @ApiOperation(value = "删除用户")
    @PreAuthorize("hasAuthority('bnt.sysUser.remove')")
    public Result deleteUser(@PathVariable("id") Long id){
        boolean b = userService.removeById(id);
        if (b){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 分页查询
    @GetMapping("/{page}/{limit}")
    @ApiOperation(value = "分页查询")
    public Result userPage(
            @ApiParam(value="page",name = "页码",required = true)
            @PathVariable("page") Long page,
            @ApiParam(value="limit",name = "每页条数",required = true)
            @PathVariable("limit") Long limit,
            SysUserQueryVo vo
            ){
        IPage<SysUser> UserPage =  userService.selectPage(page,limit,vo);
        return Result.ok(UserPage);
    }

    // 修改用户状态
    @PutMapping("/updataStatus/{userId}/{status}")
    public Result updataStatus(@PathVariable("userId") Long userId,
                               @PathVariable("status") Integer status){
        boolean isSuccess = userService.updateStatus(userId,status);
        if (isSuccess){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

}
