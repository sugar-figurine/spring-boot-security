package com.mds.system.controller;

import com.common.entity.result.Result;
import com.common.entity.vo.LoginVo;
import com.mds.system.config.security.CustomUser;
import com.mds.system.service.SysUserService;
import com.mds.system.utils.JwtHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Api(tags = "用户登录接口")
@RestController
@RequestMapping("/admin/sysRole/index")
public class IndexController {

    @Autowired
    SysUserService sysUserService;

    //login  {"code":20000,"data":{"token":"admin-token"}}
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public Result login(@RequestBody LoginVo vo){
        System.out.println("login------------->"+vo);
        return Result.ok();
    }


    //获取用户信息
    @GetMapping("/info")
    @ApiOperation(value = "登录信息")
    public Result info(HttpServletRequest request){
        // 获取token
        String token = request.getHeader("token");
        String username = JwtHelper.getUsername(token);
        Map<String, Object> userInfo = sysUserService.getUserInfo(username);
        return Result.ok(userInfo);
    }

    //login  {"code":20000,"data":{"token":"admin-token"}}
    @PostMapping("/logout")
    @ApiOperation(value = "退出登录")
    public Result logout(){
        return Result.ok();
    }
}
