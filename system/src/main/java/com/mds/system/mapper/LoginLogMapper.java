package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysLoginLog;

public interface LoginLogMapper extends BaseMapper<SysLoginLog> {
}
