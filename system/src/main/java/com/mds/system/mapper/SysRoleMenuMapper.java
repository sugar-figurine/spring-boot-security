package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysRoleMenu;

/**
 * @Entity com.mds.system..SysRoleMenu
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}




