package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysPost;

/**
 * @Entity com.mds.system.entity.SysPost
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

}




