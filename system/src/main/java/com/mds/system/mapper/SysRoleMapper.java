package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysRole;
import org.apache.ibatis.annotations.Mapper;

public interface SysRoleMapper extends BaseMapper<SysRole> {
}
