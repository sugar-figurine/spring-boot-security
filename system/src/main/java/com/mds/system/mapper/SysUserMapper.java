package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysUser;

/**
 * @Entity com.mds.system.entity.SysUser
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}




