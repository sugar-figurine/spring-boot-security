package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.mds.system..SysMenu
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> fandMenuListByUserId(@Param("userId") String userId);
}




