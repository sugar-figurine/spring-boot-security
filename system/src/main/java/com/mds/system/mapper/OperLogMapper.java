package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysLoginLog;
import com.common.entity.system.SysOperLog;

public interface OperLogMapper extends BaseMapper<SysOperLog> {
}
