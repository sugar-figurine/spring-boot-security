package com.mds.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.common.entity.system.SysUserRole;

/**
 * @Entity com.mds.system..SysUserRole
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}




