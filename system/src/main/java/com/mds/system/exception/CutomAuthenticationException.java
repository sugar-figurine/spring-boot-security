package com.mds.system.exception;

import org.springframework.security.core.AuthenticationException;

public class CutomAuthenticationException extends AuthenticationException {
    public CutomAuthenticationException(String message) {
        super(message);
    }
}
