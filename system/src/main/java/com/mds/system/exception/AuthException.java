package com.mds.system.exception;

import java.util.concurrent.Executors;

public class AuthException extends RuntimeException {
    public AuthException(String message) {
        super(message);
    }
}
