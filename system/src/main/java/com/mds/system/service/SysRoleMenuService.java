package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysMenu;
import com.common.entity.system.SysRoleMenu;
import com.common.entity.vo.AssginMenuVo;

import java.util.List;

/**
 *
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    List<SysMenu> fandAuth(Long id);

    boolean saveAssign(AssginMenuVo vo);
}
