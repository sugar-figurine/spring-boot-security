package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysPost;

/**
 *
 */
public interface SysPostService extends IService<SysPost> {

}
