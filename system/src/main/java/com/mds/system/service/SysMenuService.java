package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysMenu;

import java.util.List;

/**
 *
 */
public interface SysMenuService extends IService<SysMenu> {

    List<SysMenu> treeMenu();

    // 根据用户id查询权限
    List<SysMenu> fandMenuListByUserId(String userId);

    // 根据用户id查询权限返回树形菜单
    List<SysMenu> fandTreeMenuListByUserId(String userId);

    List<String> getUserButtonListById(String userId);
}
