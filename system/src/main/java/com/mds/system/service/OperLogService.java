package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysOperLog;

public interface OperLogService extends IService<SysOperLog> {
}
