package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysUserRole;
import com.common.entity.vo.AssginRoleVo;

import java.util.Map;

/**
 *
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    Map<String, Object> getUserRole(Long id);

    void saveUserRole(AssginRoleVo vo);
}
