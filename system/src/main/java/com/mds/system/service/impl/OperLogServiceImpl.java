package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysOperLog;
import com.mds.system.mapper.OperLogMapper;
import com.mds.system.service.OperLogService;
import org.springframework.stereotype.Service;

@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper,SysOperLog> implements OperLogService {

}
