package com.mds.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysLoginLog;
import com.mds.system.mapper.LoginLogMapper;
import com.mds.system.service.LoginLogService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper,SysLoginLog> implements LoginLogService {

}
