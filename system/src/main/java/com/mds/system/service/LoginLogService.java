package com.mds.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysLoginLog;

public interface LoginLogService extends IService<SysLoginLog> {
}
