package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.result.ResultCodeEnum;
import com.common.entity.system.SysMenu;
import com.mds.system.exception.AuthException;
import com.mds.system.service.SysMenuService;
import com.mds.system.mapper.SysMenuMapper;
import com.mds.system.utils.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
        implements SysMenuService {

    @Resource
    SysMenuMapper sysMenuMapper;

    /**
     * 查询所有的菜单，并构建树形
     *
     * @return
     */
    @Override
    public List<SysMenu> treeMenu() {
        List<SysMenu> menuList = baseMapper.selectList(null);
        return TreeUtils.handlerTree(menuList);
    }

    /**
     * 根据用户id查询权限
     *
     * @param userId
     * @return
     */
    @Override
    public List<SysMenu> fandMenuListByUserId(String userId) {
        if (userId != null && userId != "") {
            List<SysMenu> sysMenus = sysMenuMapper.fandMenuListByUserId(userId);
            return sysMenus;
        }
        return null;
    }

    /**
     * 根据用户id查询权限 返回树形菜单
     *
     * @param userId
     * @return
     */
    @Override
    public List<SysMenu> fandTreeMenuListByUserId(String userId) {
        if (userId != null && userId != "") {
            List<SysMenu> sysMenus = sysMenuMapper.fandMenuListByUserId(userId);
            return TreeUtils.handlerTree(sysMenus);
        }
        return null;
    }

    @Override
    public List<String> getUserButtonListById(String userId) {
        List<SysMenu> sysMenus = sysMenuMapper.fandMenuListByUserId(userId);
        List<String> perms =
                sysMenus.stream()
                            .filter(item -> {
                                    return item.getType() == 2;
                                })
                            .map(SysMenu::getPerms)
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList());
        return perms;
    }

    @Override
    public boolean removeById(Serializable id) {
        // 查看当前id是否有子菜单
        Integer count = this.count(new QueryWrapper<SysMenu>().eq("parent_id", id));
        if (count > 0) {
            throw new AuthException(ResultCodeEnum.NODE_ERROR.getMessage());
        }
        int num = baseMapper.deleteById(id);
        if (num > 0) {
            return true;
        }
        return false;
    }
}




