package com.mds.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysPost;
import com.mds.system.service.SysPostService;
import com.mds.system.mapper.SysPostMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost>
    implements SysPostService{

}




