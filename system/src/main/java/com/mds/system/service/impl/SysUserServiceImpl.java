package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysMenu;
import com.common.entity.system.SysUser;
import com.common.entity.vo.RouterVo;
import com.common.entity.vo.SysUserQueryVo;
import com.mds.system.service.SysMenuService;
import com.mds.system.service.SysUserService;
import com.mds.system.mapper.SysUserMapper;
import com.mds.system.utils.RouterHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
        implements SysUserService {

    @Autowired
    SysMenuService menuService;

    /**
     * 分页查询
     *
     * @param page
     * @param limit
     * @param vo
     * @return
     */
    @Override
    public IPage<SysUser> selectPage(Long page, Long limit, SysUserQueryVo vo) {
        Page<SysUser> sysUserPage = new Page<SysUser>(page, limit);
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        if (vo.getKeyword() != null && vo.getKeyword() != "") {
            wrapper.like("username", vo.getKeyword())
                    .or()
                    .like("name", vo.getKeyword())
                    .or()
                    .like("phone", vo.getKeyword());
        }
        if (vo.getCreateTimeBegin() != null && vo.getCreateTimeBegin() != "") {
            wrapper.ge("create_time", vo.getCreateTimeBegin());
        }
        if (vo.getCreateTimeEnd() != null && vo.getCreateTimeEnd() != "") {
            wrapper.le("create_time", vo.getCreateTimeEnd());
        }
        IPage<SysUser> userPage = baseMapper.selectPage(sysUserPage, wrapper);
        return userPage;
    }

    @Override
    public boolean updateStatus(Long userId, Integer status) {
        SysUser user = baseMapper.selectById(userId);
        user.setStatus(status);
        int count = baseMapper.updateById(user);
        return count > 0 ? true : false;
    }

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    @Override
    public SysUser fandUserByUsername(String username) {
        if (username!=null && username!=""){
            // 根据用户名查询用户
            SysUser sysUser = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("username", username));
            return sysUser;
        }
        return null;
    }

    @Override
    public Map<String, Object> getUserInfo(String username) {
        SysUser sysUser = fandUserByUsername(username);
        if (sysUser == null){
            throw new RuntimeException("查询信息失败！");
        }
        // 根据userid 查询菜单 树形菜单
        List<SysMenu> sysMenus = menuService.fandTreeMenuListByUserId(sysUser.getId().toString());
        //转换成前端路由要求格式数据
        List<RouterVo> routerVoList = RouterHelper.buildRouters(sysMenus);
        // 用户按钮权限
        List<String> permsList = menuService.getUserButtonListById(sysUser.getId().toString());
        HashMap<String, Object> map = new HashMap<>();
        map.put("roles","[admin]");
        map.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        map.put("name",username);
        map.put("routers", routerVoList);
        map.put("buttons", permsList);
        return map;
    }
}




