package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysRole;
import com.common.entity.vo.SysRoleQueryVo;
import com.mds.system.mapper.SysRoleMapper;
import com.mds.system.service.SysRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    public void test(){
        List<SysRole> list = this.list();
    }

    @Override
    public IPage<SysRole> selectPage(Page<SysRole> pageParam, SysRoleQueryVo roleQueryVo) {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        if (roleQueryVo.getRoleName() != null && !roleQueryVo.getRoleName().equals("")){
            wrapper.like("role_name", "%"+roleQueryVo.getRoleName()+"%");
        }
        IPage<SysRole> page = this.page(pageParam, wrapper);
        return page;
    }
}
