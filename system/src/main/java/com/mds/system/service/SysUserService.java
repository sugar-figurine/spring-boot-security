package com.mds.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.common.entity.system.SysUser;
import com.common.entity.vo.SysUserQueryVo;

import java.util.Map;

/**
 *
 */
public interface SysUserService extends IService<SysUser> {

    IPage<SysUser> selectPage(Long page, Long limit, SysUserQueryVo vo);

    boolean updateStatus(Long userId, Integer status);

    SysUser fandUserByUsername(String username);

    Map<String, Object> getUserInfo(String username);
}
