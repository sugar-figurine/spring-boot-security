package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysRole;
import com.common.entity.system.SysUserRole;
import com.common.entity.vo.AssginRoleVo;
import com.mds.system.service.SysRoleService;
import com.mds.system.service.SysUserRoleService;
import com.mds.system.mapper.SysUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
        implements SysUserRoleService {

    @Autowired
    SysRoleService roleService;

    @Override
    public Map<String, Object> getUserRole(Long id) {
        // 获取所有的角色
        List<SysRole> roleList = roleService.list();
        // 查询用户拥有的角色
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        if (id != null) {
            wrapper.eq("user_id", id);
        }
        // 获取当前用户已分配的角色
        List<SysUserRole> userRoles = baseMapper.selectList(wrapper);
        // 处理一下，前端只需要角色的id
        List<String> roleIds = userRoles.stream()
                .map(SysUserRole::getRoleId)
                .collect(Collectors.toList());
        // 封装返回
        HashMap<String, Object> map = new HashMap<>();
        map.put("allRoles", roleList);
        map.put("userRoleIds", roleIds);
        return map;
    }

    @Override
    public void saveUserRole(AssginRoleVo vo) {
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        // 将原来分配的删掉
        if (vo.getUserId()!=null && vo.getUserId()!=""){
            wrapper.eq("user_id", vo.getUserId());
        }
        baseMapper.delete(wrapper);
        List<String> roleIdList = vo.getRoleIdList();
        if (!roleIdList.isEmpty()){
            roleIdList.forEach(v ->{
                SysUserRole userRole = new SysUserRole();
                userRole.setUserId(vo.getUserId());
                userRole.setRoleId(v);
                // 保存
                baseMapper.insert(userRole);
            });
        }
    }
}




