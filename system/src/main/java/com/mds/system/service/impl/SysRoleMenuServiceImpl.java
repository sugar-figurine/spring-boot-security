package com.mds.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.entity.system.SysMenu;
import com.common.entity.system.SysRoleMenu;
import com.common.entity.vo.AssginMenuVo;
import com.mds.system.exception.AuthException;
import com.mds.system.service.SysMenuService;
import com.mds.system.service.SysRoleMenuService;
import com.mds.system.mapper.SysRoleMenuMapper;
import com.mds.system.utils.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
    implements SysRoleMenuService{

    @Autowired
    SysMenuService menuService;

    @Override
    public List<SysMenu> fandAuth(Long id) {
        // 查询所有的菜单权限
        List<SysMenu> list = menuService.list();
        // 查询角色拥有的菜单权限
        List<SysRoleMenu> roleMenus = this.list(new QueryWrapper<SysRoleMenu>().eq("role_id", id));
        // 只保留menu id
        List<String> menuIds = roleMenus.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        // 通过角色拥有的权限 设置SysMenu 的isSelect 属性
        list.forEach(sysMenu -> {
            if (menuIds.contains(sysMenu.getId().toString())){
                sysMenu.setSelect(true);
            }else {
                sysMenu.setSelect(false);
            }
        });
        // 组装成树形结构
        return TreeUtils.handlerTree(list);
    }

    @Override
    public boolean saveAssign(AssginMenuVo vo) {
        // 将原来的权限分配关系删掉
        String roleId = vo.getRoleId();
        if (roleId == null && roleId == ""){
            throw new AuthException("角色id不能为空");
        }
        this.remove(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId));
        // 需要保存的 menuid
        List<String> menuIdList = vo.getMenuIdList();
        menuIdList.forEach(s -> {
            SysRoleMenu roleMenu = new SysRoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(s);
            this.save(roleMenu);
        });
        return true;
    }
}




